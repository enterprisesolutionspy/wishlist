# IONIC APP - Lista de deseos / tareas
```
Requiere:
NPM
IONIC 
```

# Descripción
```
Creación de tareas en formato de listas.
Cada lista puede tener sus propios items dentro de la misma lista
Separado por tareas pendientes y terminados
```

# Capturas
![pendiente](https://i.ibb.co/NL0pJHg/pendientes.png "pendiente")
![lista](https://i.ibb.co/zF1p0Lt/nueva-lista.png "lista")
![items](https://i.ibb.co/sJS98NM/items.png "items")